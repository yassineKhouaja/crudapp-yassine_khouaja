const express = require("express");
const projectRoutes = require("./routes/projectRoutes");

const app = express();

//MIDDLEWARES
app.use(express.json());

app.use("/api/v1/projects", projectRoutes);

module.exports = app;
