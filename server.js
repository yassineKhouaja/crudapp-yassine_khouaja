const app = require("./app");

const port = 3000;

//   START SERVER
const server = app.listen(port, () => {
  console.clear();
  console.log(`app running on port ${port}`);
});
