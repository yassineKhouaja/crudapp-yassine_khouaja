const fs = require("fs");
const ApiActions = require("../utils/ApiActions");
const catchAsync = require("../utils/catchAsync");

const projects = JSON.parse(
  fs.readFileSync(`${__dirname}/../data/projects.json`)
);

const project = new ApiActions(projects);

exports.getAllProjects = catchAsync(async (req, res, next) => {
  console.log(req.query.task);
  res.status(200).json({
    status: "success",
    results: project.getAll(req.query.task).length,
    data: {
      projects: project.getAll(req.query.task),
    },
  });
});

exports.getProject = (req, res, next) => {
  const result = project.getOne(req);
  if (result) {
    res.status(200).json({
      status: "success",
      data: {
        projects: result,
      },
    });
  } else {
    res.status(404).json({
      status: "fail",
      messge: "there is no project with this Id and has tasks",
    });
  }
};

exports.deleteProjects = (req, res, next) => {
  project.deleteDone();

  res.status(200).json({
    status: "success",
    messge: "all done projects are deleted",
  });
};
exports.updateProject = (req, res, next) => {
  const UpdateProject = project.updateOne(req);

  res.status(200).json({
    status: "success",
    messge: "the tasks are updated",
    data: { UpdateProject },
  });
};

exports.addProject = (req, res, next) => {
  const UpdateProject = project.createOne(req);

  res.status(200).json({
    status: "success",
    data: { UpdateProject },
  });
};
