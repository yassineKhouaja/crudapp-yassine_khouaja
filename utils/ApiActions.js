class ApiActions {
  constructor(data) {
    this.data = [...data];
    this.projectCount = data.length + 1;
  }
  getAll(task) {
    // return all projects if there no filter
    if (!task) {
      return this.data;
    }
    // return all projects with tasks
    if (task === "yes") {
      return this.data.filter((project) => project.tasks?.length);
    }
    // return all projects without tasks

    if (task === "no") {
      return this.data.filter((project) => !project.tasks?.length);
    }
  }
  // return  project with tasks
  getOne(req) {
    const id = req.params.id;
    const project = this.data.find(
      (project) => project.id === id && project.tasks?.length > 0
    );
    return project;
  }
  // replace tasks of the id with tasks given into the request body
  updateOne(req) {
    const id = req.params.id;

    const index = this.data.findIndex((el) => el.id === id);

    this.data[index].tasks = [...req.body.tasks];
    return this.data[index];
  }
  // delete all projects done
  deleteDone() {
    const remainingProjects = this.data.filter(
      (project) => project.status.trim() !== "done"
    );
    this.data = remainingProjects;
  }
  // add new project by using the data given into the request body
  createOne(req) {
    const newProject = {
      id: `p${this.projectCount}`,
      ...req.body.project,
    };
    this.data.push(newProject);
    this.projectCount = this.projectCount + 1;
    return newProject;
  }
}

module.exports = ApiActions;
