const express = require("express");
const {
  getAllProjects,
  getProject,
  deleteProjects,
  updateProject,
  addProject,
} = require("../controllers/projectController");

const router = express.Router();

router.route("/").get(getAllProjects).post(addProject).delete(deleteProjects);
router.route("/:id").get(getProject).post(updateProject);

module.exports = router;
